SQL CRUD OPERATIONS


1. (Create) Adding a Record
	Terminal
	Adding a Record:
	Syntax
		INSERT INTO table_name (column_name) VALUES (values1);
	Example
		INSERT INTO artists (name) VALUES ("Nirvana");
		INSERT INTO artists (name) VALUES ("Taylor Swift");
		INSERT INTO artists (name) VALUES ("Ed Sheeran");

2. (Read) Show all records 
	Terminal
		Displaying / retrieving records
		Syntax
			SELECT column_name FROM table_name;
		Example
			SELECT name FROM artists;

3. (Create) Adding a record with multiple columns
	Terminal
		Adding a record with multiple columns 
		Syntax
			INSERT INTO table_name (column_name, column_name) VALUES (values1, values2);
		Example
			INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Nevermind", "1991-09-24", 1);
			INSERT INTO albums (album_title, date_released, artist_id) VALUES ("All Too Well", "2012-09-24", 2);
			INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Perfect", "2017-09-24", 3);

4. Adding multiple records
	Terminal
	Adding multiple records
	Syntax
	INSERT INTO table_name (column_name, column_name) VALUES (values1, values2), (values3, values4);
	Example
	INSERT INTO songs (song_name, lenght, genre, album_id) VALUES ("Smell like A Teen Spirit", 501, "Grunge", 1);
	INSERT INTO songs (song_name, lenght, genre, album_id) VALUES ("Love Story", 357, "Pop", 2);
	INSERT INTO songs (song_name, lenght, genre, album_id) VALUES ("Shape of You", 424, "Pop", 3);

5. Show record with selected columns
	Terminal
	Retrieving records with selected columns
	Syntax
	SELECT (column_name1, column_name2) FROM table_name;
	SELECT song_name FROM songs;
	SELECT song_name, genre FROM songs;
	SELECT album_title, date_released FROM albums;

	SELECT * FROM songs;
	-- will show the full table

6. Show records that meet a certain conditions
	Terminal 
		Retrieving records with certain conditions
	Syntax 
		SELECT column_name FROM table_name WHERE condition;
	Example
		SELECT song_name FROM songs WHERE genre = "Pop";

7. Show records with multiple conditions
	Terminal
		Displaying / retrieving records with multiple conditions
	Syntax
		AND CLAUSE
		SELECT column_name FROM table_name WHERE condition1 AND condition2;

		OR CLAUSE
		SELECT column_name FROM table_name WHERE condition1 OR condition2;

8. (Update) Updating Records
	Terminal

	Add a record to update:
		INSERT INTO artists (name) VALUES ("Incubus");
		INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Monuments and Melodies", "2009-06-16", 4);
		INSERT INTO songs (song_name, lenght, genre, album_id) VALUES ("Megalomaniac", 410, "Classical", 4);

	Updating Records:
		UPDATE table_name SET column_name = value WHERE condition;
		UPDATE table_name SET column_name = value1, column_name2 = value2  WHERE  condition;
		-- when updating or deleting, and a WHERE clause or else you may update or delete all items in a table
		UPDATE songs SET genre = "Rock" WHERE lenght > 400;

9. (Delete) Deleting records
	Terminal
		Deleting records
		Syntax
			DELETE FROM table_name WHERE condition;
			DELETE FROM songs WHERE genre = "ROCK" AND  lenght > 400;
		-- removing where clause will delete all rows

	 